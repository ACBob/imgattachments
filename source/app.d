import std.stdio;
import std.file;
import std.string;
import std.digest.crc;
import std.getopt;
import core.stdc.string;

import imageformats;

// TODO: Different formats than just RGB.

enum WrittenMode : ubyte
{
	Raw, // Blindly written to the image.
	Dither2nd, // Every other pixel is written to, on-top of a source image.
	Dither4th, // Every other other pixel is written to on-top of a source image.
}

static const char[6] ATTACHED_FILE_MAGIC = "BLINGO";

align(1) struct AttachedFile
{
	// magic to show what file this is.
	const char[6] MAGIC;

	char[16] name; // NUL or Size terminated 16-character name string.
	ubyte[4] crc; // A checksum to test the file against.
	WrittenMode mode; // The mode used to write this.

	ulong payload; // Size of payload.

	// End of header data; assume rest is payload-sized data.
	// Can get the size of the header from data.offsetof
	ubyte[] data;
}

/// Figures out the optimal image size in pixels to contain a buffer.
/// Params:
///   l = The size of the buffer.
///   x = Somewhere to put width.
///   y = Somewhere to put height
void optimal_size(ulong l, ref int x, ref int y)
{
	import std.math;

	x = cast(ushort) sqrt(cast(float) l);
	y = cast(ushort) ceil((l / cast(float) x));
}

ulong size_needed(AttachedFile file)
{
	final switch (file.mode)
	{
	case WrittenMode.Raw:
		return file.payload + file.data.offsetof;
	case WrittenMode.Dither2nd: // Dither2nd requires 3x the space because of the dithering.
		return (file.payload * 3) + file.data.offsetof;
	case WrittenMode.Dither4th: // Dither4th requires 6x the space because of the dithering.
		return (file.payload * 6) + file.data.offsetof;
	}
}

/// Creates a brand new image from the given attached file.
/// NOTE: This function can only create Raw images.
/// Params:
///   file = A filled AttachedFile struct to plop into the image.
///   mode = The mode to use.
/// Returns: A created image.
IFImage create_img_from_file(AttachedFile file)
{
	IFImage img;

	// Figure out the size we want.
	// We force the mode to raw here.
	file.mode = WrittenMode.Raw;
	ulong sizeOfFile = size_needed(file);
	optimal_size((sizeOfFile / 3) + sizeOfFile % 3, img.w, img.h);
	writeln("size ", sizeOfFile);
	img.c = ColFmt.RGB;

	// We want to then write our data.
	img.pixels = new ubyte[img.w * img.h * 3];

	// Write our header.
	memcpy(cast(void*) img.pixels.ptr, cast(void*)&file, file.data.offsetof);

	// Now we write our data.
	memcpy(cast(void*) img.pixels.ptr + file.data.offsetof, cast(void*) file.data.ptr, file.payload);

	return img;
}

IFImage embed_file_on_img(AttachedFile file, IFImage src)
{
	IFImage img;

	// Figure out the size we want.
	ulong sizeOfFile = size_needed(file);
	writeln("size ", sizeOfFile, " vs ", src.pixels.length);
	img.c = ColFmt.RGB;

	// We want to see if the image coming in is big enough.
	if (src.pixels.length < sizeOfFile)
		throw new Error("Image not large enough to store!");

	// We want to then write our data.
	img.w = src.w;
	img.h = src.h;
	img.pixels = src.pixels.dup;

	// Write our header.
	memcpy(cast(void*) img.pixels.ptr, cast(void*)&file, file.data.offsetof);

	// Now we write our data.
	final switch (file.mode)
	{
	case WrittenMode.Raw:
		// Raw just splats the image data on-top.
		memcpy(cast(void*) img.pixels.ptr + file.data.offsetof,
			cast(void*) file.data.ptr,
			file.payload);
		break;
	case WrittenMode.Dither2nd:
		for (ulong i = 0; i < file.payload - 3; i += 3)
		{
			img.pixels[file.data.offsetof + (i * 3) + 0] = file.data[i + 0];
			img.pixels[file.data.offsetof + (i * 3) + 1] = file.data[i + 1];
			img.pixels[file.data.offsetof + (i * 3) + 2] = file.data[i + 2];
		}
		break;
	case WrittenMode.Dither4th:
		for (ulong i = 0; i < file.payload - 3; i += 3)
		{
			img.pixels[file.data.offsetof + (i * 6) + 0] = file.data[i + 0];
			img.pixels[file.data.offsetof + (i * 6) + 1] = file.data[i + 1];
			img.pixels[file.data.offsetof + (i * 6) + 2] = file.data[i + 2];
		}
		break;
	}

	return img;
}

/// Decodes an image down to a file.
/// Params:
///   img = The image to decode.
///   ignoreCRC = If true, CRC checksums will be ignored when decoding.
/// Returns: an AttachedFile containing the decoded file.
AttachedFile decode_file_from_img(IFImage img, bool ignoreCRC = false)
{
	AttachedFile file;

	// Let's test the magic first.
	if (memcmp(
			cast(void*) img.pixels.ptr,
			cast(void*) ATTACHED_FILE_MAGIC,
			ATTACHED_FILE_MAGIC.sizeof) != 0)
		throw new Error("Invalid Magic!");

	// Now we know magic is fine, let's read the header data.
	memcpy(cast(void*)&file, cast(void*) img.pixels.ptr, file.data.offsetof);

	// Verify our payload's size.
	if (file.payload > img.pixels.length - file.data.offsetof)
		throw new Error("File payload too large to make sense!");

	// Since it's valid, we can now read the data.
	file.data = new ubyte[file.payload];

	final switch (file.mode)
	{
	case WrittenMode.Raw:
		memcpy(cast(void*) file.data, cast(void*) img.pixels.ptr + file.data.offsetof, file.payload);
		break;
	case WrittenMode.Dither2nd:
		for (ulong i = 0; i < file.payload - 3; i += 3)
		{
			file.data[i + 0] = img.pixels[file.data.offsetof + (i * 3) + 0];
			file.data[i + 1] = img.pixels[file.data.offsetof + (i * 3) + 1];
			file.data[i + 2] = img.pixels[file.data.offsetof + (i * 3) + 2];
		}
		break;
	case WrittenMode.Dither4th:
		for (ulong i = 0; i < file.payload - 3; i += 3)
		{
			file.data[i + 0] = img.pixels[file.data.offsetof + (i * 6) + 0];
			file.data[i + 1] = img.pixels[file.data.offsetof + (i * 6) + 1];
			file.data[i + 2] = img.pixels[file.data.offsetof + (i * 6) + 2];
		}
		break;
	}

	// Now we can test the checksum!
	auto checksum = crc32Of(file.data);
	if (!ignoreCRC && checksum != file.crc)
		throw new Error("File checksum mismatch!");

	return file;
}

void main(string[] args)
{
	string input;
	string output;
	string bg;
	bool extract;
	bool noverify;
	auto opt = getopt(
		args,

		std.getopt.config.required,
		"input|i", "The path to the input file.", &input,
		"output|o", "The path to the output file.", &output,
		"bg", "The background image.", &bg,

		"extract|x", "If specified, will attempt to extract instead of create.", &extract,
		"no-verify", "If specified, CRC Checksums will be ignored.", &noverify,
	);

	if (opt.helpWanted)
	{
		defaultGetoptPrinter("A silly program to hide files inside PNGs.", opt.options);
		return;
	}

	if (extract)
	{
		auto read = read_image(input, ColFmt.RGB);
		auto decoded = decode_file_from_img(read, noverify);

		if (output.length == 0)
		{
			import std.conv;

			output = to!string("out_" ~ decoded.name);
		}

		std.file.write(output, decoded.data);
	}
	else
	{
		auto file = cast(ubyte[]) std.file.read(input);

		// we want to truncate the name.
		char[16] name = input;

		AttachedFile filedata =
		{
			ATTACHED_FILE_MAGIC,
			name,
			crc32Of(file),
			WrittenMode.Dither4th,
			file.length,
			file
		};

		if (output.length == 0)
			output = input ~ ".png";

		if (bg.length == 0)
		{
			auto encoded = create_img_from_file(filedata);
			write_image(output, encoded.w, encoded.h, encoded.pixels, encoded.c);
		}
		else
		{
			auto background = read_image(bg, ColFmt.RGB);
			auto encoded = embed_file_on_img(filedata, background);
			write_image(output, encoded.w, encoded.h, encoded.pixels, encoded.c);
		}
	}
}
